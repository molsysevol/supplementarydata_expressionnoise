# enoise 1.2
# Noise evolution program

Usage: enoise networkFile paramFile outfile [-continue]

## Non-optional features:
- All genes under selection
- All genes have the same starting level of noise
- Fixed network size (NUM_NODES)
- Mutations in noise genotypes
- Output: a summarized table with NUM_OUT_GENS rows and (2 x NUM_NODES + 2) columns,
containing summarized genotypes, phenotypes and fitness of all individuals.

summarizedGPF:
| generation | mean G1 in pop  | mean G2 in pop | ... | var P1 in pop  | var P2 in pop  | ... | mean fitness |
| ---------- | --------------  | -------------- | ... | -------------- | -------------- | ... | ------------ |
| --- 1 ---- | ---- 100 -----  | ---- 100 ----- | ... | ---- 60.5 ---- | ---- 32.6 ---- | ... | -- 0.001 --- |
| --- 50 --- | ---- 90.3 ----  | ---- 30.2 ---- | ... | ---- 59.5 ---- | ---- 33.4 ---- | ... | -- 0.003 --- |
| ---------- | --------------  | -------------- | ... | -------------- | -------------- | ... | ------------ |
| - 10000 -- | ---- 5.02 ----  | ---- 10.5 ---- | ... | ---- 60.1 ---- | ---- 34.0 ---- | ... | ---- 0.2 --- |

## Optional features:
- Optimal expression levels
- Stochastic or deterministic realization (input noise levels with STARTING_NOISE_INT_ALL)
