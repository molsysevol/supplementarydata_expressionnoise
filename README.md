# SupplementaryData_ExpressionNoise
This repository contains the code to reproduce the results of our preprint titled:

Being noisy in a crowd: differential selective pressure on gene expression noise in model gene regulatory networks 

We used evolutionary simulations to study the evolution of gene-specific expression noise in gene regulatory networks and found that local and global network properties affect the evolvability of gene-specific expression noise.

## Installation and data download
Clone this repo:

```
$ git clone git@gitlab.gwdg.de:molsysevol/supplementarydata_expressionnoise.git YOUR_PROJECT_NAME
$ cd YOUR_PROJECT_NAME
```

Download the zipped raw data from zenodo (link) in this folder and unzip.

```
$ unzip results.zip
```

## Reproducing the results
The full code to reproduce the results in the maintext, supplementary information, and additional tests has been compiled into a shell script called pipe_analysis.sh.
Run the analysis script:
```
$ ./pipe_analysis.sh
```
Running the script will knit Rmd files into HTML files and output figures in the plots folder.

## Regenerating the simulation data
The scripts to generate network topologies, perform network analysis, calculate noise metrics and preprocess the data have been compiled into a shell script called pipe_regenerate_data.sh.
Note that to regenerate the simulation data you will have to install the simulator and adapt the scripts to your HPC system.

## Simulator
The version of the code used for running the simulations presented in the paper is found in cpp. The program is not maintained here, for the latest version please go to https://github.com/NatashaPuzovic/netlings. 
